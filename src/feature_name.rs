use clippy_utils::diagnostics::span_lint_and_help;
use clippy_utils::{diagnostics::span_lint, is_lint_allowed};
use rustc_hir::{Crate, CRATE_HIR_ID};
use rustc_lint::{LateContext, LateLintPass};
use rustc_session::{declare_lint_pass, declare_tool_lint};
use rustc_span::source_map::DUMMY_SP;

declare_clippy_lint! {
    /// ### What it does
    /// Checks for feature names with prefix `use-`, `with-` or postfix `-support`
    ///
    /// ### Why is this bad?
    /// These prefixes and postfixes have no significant meaning.
    ///
    /// ### Example
    /// ```toml
    /// # The `Cargo.toml` with feature name redundancy
    /// [features]
    /// default = ["use-abc", "with-def", "ghi-support"]
    /// use-abc = []  // redundant
    /// with-def = []   // redundant
    /// ghi-support = []   // redundant
    /// ```
    ///
    /// Use instead:
    /// ```toml
    /// [features]
    /// default = ["abc", "def", "ghi"]
    /// abc = []
    /// def = []
    /// ghi = []
    /// ```
    ///
    pub REDUNDANT_FEATURE_NAMES,
    cargo,
    "usage of a redundant feature name"
}

declare_clippy_lint! {
    /// ### What it does
    /// Checks for negative feature names with prefix `no-` or `not-`
    ///
    /// ### Why is this bad?
    /// Features are supposed to be additive, and negatively-named features violate it.
    ///
    /// ### Example
    /// ```toml
    /// # The `Cargo.toml` with negative feature names
    /// [features]
    /// default = []
    /// no-abc = []
    /// not-def = []
    ///
    /// ```
    /// Use instead:
    /// ```toml
    /// [features]
    /// default = ["abc", "def"]
    /// abc = []
    /// def = []
    ///
    /// ```
    pub NEGATIVE_FEATURE_NAMES,
    cargo,
    "usage of a negative feature name"
}

declare_lint_pass!(FeatureName => [REDUNDANT_FEATURE_NAMES, NEGATIVE_FEATURE_NAMES]);

static PREFIXES: [&str; 8] = ["no-", "no_", "not-", "not_", "use-", "use_", "with-", "with_"];
static POSTFIXES: [&str; 2] = ["-support", "_support"];

fn is_negative_prefix(s: &str) -> bool {
    s.starts_with("no") || s.starts_with("not")
}

impl LateLintPass<'_> for FeatureName {
    fn check_crate(&mut self, cx: &LateContext<'_>, _: &Crate<'_>) {
        let mut sorted_prefixes = PREFIXES;
        sorted_prefixes.sort_unstable();
        assert_eq!(PREFIXES, sorted_prefixes);
        let mut sorted_postfixes = POSTFIXES;
        sorted_postfixes.sort_unstable();
        assert_eq!(POSTFIXES, sorted_postfixes);

        if is_lint_allowed(cx, REDUNDANT_FEATURE_NAMES, CRATE_HIR_ID)
            && is_lint_allowed(cx, NEGATIVE_FEATURE_NAMES, CRATE_HIR_ID)
        {
            return;
        }

        let metadata = unwrap_cargo_metadata!(cx, REDUNDANT_FEATURE_NAMES, false);

        for package in metadata.packages {
            let mut features: Vec<&String> = package.features.keys().collect();
            features.sort();
            for feature in features {
                let i = PREFIXES.partition_point(|prefix| prefix < &feature.as_str());
                if i > 0 && feature.starts_with(PREFIXES[i - 1]) {
                    if is_negative_prefix(PREFIXES[i - 1]) {
                        span_lint(
                            cx,
                            NEGATIVE_FEATURE_NAMES,
                            DUMMY_SP,
                            &format!(
                                "the \"{}\" prefix in the feature name \"{}\" is negative",
                                PREFIXES[i - 1],
                                feature
                            ),
                        );
                    } else {
                        span_lint_and_help(
                            cx,
                            REDUNDANT_FEATURE_NAMES,
                            DUMMY_SP,
                            &format!(
                                "the \"{}\" prefix in the feature name \"{}\" is redundant",
                                PREFIXES[i - 1],
                                feature
                            ),
                            None,
                            &format!(
                                "consider renaming the feature to \"{}\"",
                                feature.strip_prefix(PREFIXES[i - 1]).unwrap()
                            ),
                        );
                    }
                }
                for postfix in &POSTFIXES {
                    if feature.ends_with(postfix) {
                        span_lint_and_help(
                            cx,
                            REDUNDANT_FEATURE_NAMES,
                            DUMMY_SP,
                            &format!(
                                "the \"{}\" postfix in the feature name \"{}\" is redundant",
                                postfix, feature
                            ),
                            None,
                            &format!(
                                "consider renaming the feature to \"{}\"",
                                feature.strip_suffix(postfix).unwrap()
                            ),
                        );
                        break;
                    }
                }
            }
        }
    }
}
